var avgSalaryTogether = 0;
var medianSalary = 0;
var avgSalarySeparetly = [0, 0, 0];
var sumCars = 0;
var higherSalary = [];

function getData(name) {
  $.getJSON("pracownicy.json", function(json) {
    var sum = [0, 0, 0],
        count = [0, 0, 0],
        median = [];

    for (i = 0; i < json.length; i++) {
      console.log(i);
      switch (json[i].Stanowisko) {
        case 'Dyrektor':
          if (json[i].Auto == true)
            sumCars++;
          sum[0] = sum[0] + json[i].Pensja;
          count[0]++;
          median.push(json[i].Pensja);
          higherSalary.push([(json[i].Imie + ' ' + json[i].Nazwisko), (json[i].Pensja * 0.01) * json[i].Nadgodziny])
          break;
        case 'Kierownik':
          if (json[i].Auto == true)
            sumCars++;
          sum[1] = sum[1] + json[i].Pensja;
          count[1]++;
          median.push(json[i].Pensja);
          higherSalary.push([(json[i].Imie + ' ' + json[i].Nazwisko), (json[i].Pensja * 0.01) * json[i].Nadgodziny])
          break;
        case 'Pracownik':
          if (json[i].Auto == true)
            sumCars++;
          sum[2] = sum[2] + json[i].Pensja;
          count[2]++;
          median.push(json[i].Pensja);
          higherSalary.push([(json[i].Imie + ' ' + json[i].Nazwisko), (json[i].Pensja * 0.01) * json[i].Nadgodziny])
          break;
      }
    }
    avgSalaryTogether = (sum[0] + sum[1] + sum[2]) / (count[0] + count[1] + count[2]),
    avgSalarySeparetly[0] = sum[0]/count[0],
    avgSalarySeparetly[1] = sum[1]/count[1],
    avgSalarySeparetly[2] = sum[2]/count[2];
    median.sort();
    if (median.length % 2 == 1) {
      medianSalary = median[median.length + 1] / 2;
    }
    else {
      medianSalary = (median[median.length / 2] + median[median.length / 2 + 1]) / 2;
    }
    return avgSalaryTogether, medianSalary, avgSalarySeparetly, sumCars, higherSalary;
  })
};

$(document).ready(function() {
  getData();

  $(document).on('change', '#select', function() {
    var HTML = '<div class="table-responsive">\
    <table class="table" id="pracownicy">';
    switch ($('#select').val()) {
      case 'avgSalaryTogether':
        HTML += '<thead>\
          <th>Typ</th>\
          <th>Wartosc</th>\
        </thead>\
        <tbody>\
        <tr><td>Srednia</td><td>' + avgSalaryTogether.toFixed(2) + '</td></tr>\
        <tr><td>Mediana</td><td>' + medianSalary.toFixed(2) + '</td></tr>\
        </tbody></table></div>';
        $('#table_div').html(HTML);
        break;
      case 'avgSalarySeparetly':
      HTML += '<thead>\
          <th>Stanowisko</th>\
          <th>Wartosc</th>\
        </thead>\
        <tbody>\
        <tr><td>Dyrektor</td><td>' + avgSalarySeparetly[0].toFixed(2) + '</td></tr>\
        <tr><td>Kierownik</td><td>' + avgSalarySeparetly[1].toFixed(2) + '</td></tr>\
        <tr><td>Pracownik</td><td>' + avgSalarySeparetly[2].toFixed(2) + '</td></tr>\
        </tbody></table></div>';
        $('#table_div').html(HTML);
        break;
      case 'sumCars':
        HTML += '<thead>\
          <th>Typ</th>\
          <th>Wartosc</th>\
        </thead>\
        <tbody>\
        <tr><td>Samochod</td><td>' + sumCars + '</td></tr>\
        </tbody></table></div>';
        $('#table_div').html(HTML);
        break;
      case 'higherSalary':
        HTML += '<thead>\
          <th>Imie i nazwisko</th>\
          <th>Wartosc</th>\
        </thead>\
        <tbody>';
        for (i = 0; i < higherSalary.length; i++) {
          HTML += '<tr><td>' + higherSalary[i][0] + '</td>\
          <td>' + higherSalary[i][1].toFixed(2) + '</td></tr>';
        }
        //$.each(higherSalary['name']['salary'], function(index, value) {
        //  HTML += '<tr><td>Imie nazwisko</td><td>' + value[1].toFixed(2) + "</td></tr>";
        //});
        HTML += '</tbody></table></div>';
        $('#table_div').html(HTML);
        break;
    }
  })
})
