// google.visualization.arrayToDataTable();
var employeesPercent = [];
var taxesPercent = [];
var carsPercent = [];
var overtimePercent = [];

function getData() {
  employeesPercent = [
    ['Position', 'Value'],
    ['Dyrektor', 0],
    ['Kierownik', 0],
    ['Pracownik', 0]
  ];

  taxesPercent = [
    ['Position', 'Value'],
    ['>= 7100 PLN', 0],
    ['<  7100 PLN', 0],
  ];

  carsPercent = [
    ['Position', 'Value'],
    ['Dyrektor', 0],
    ['Kierownik', 0],
    ['Pracownik', 0]
  ];

  overtimePercent = [
    ['Position', 'Value'],
    ['Dyrektor', 0],
    ['Kierownik', 0],
    ['Pracownik', 0]
  ];

  $.getJSON("pracownicy.json", function(json) {
    for (i = 0; i < json.length; i++) {
      switch (json[i].Stanowisko) {
        case 'Dyrektor':
          employeesPercent[1][1] = employeesPercent[1][1] + 1;
          overtimePercent[1][1] = overtimePercent[1][1] + json[i].Nadgodziny;
          if (json[i].Auto == true)
            carsPercent[1][1] = carsPercent[1][1] + 1;
          break;
        case 'Kierownik':
          employeesPercent[2][1] = employeesPercent[2][1] + 1;
          overtimePercent[2][1] = overtimePercent[2][1] + json[i].Nadgodziny;
          if (json[i].Auto == true)
            carsPercent[2][1] = carsPercent[2][1] + 1;
          break;
        case 'Pracownik':
          employeesPercent[3][1] = employeesPercent[3][1] + 1;
          overtimePercent[3][1] = overtimePercent[3][1] + json[i].Nadgodziny;
          if (json[i].Auto == true)
            carsPercent[3][1] = carsPercent[3][1] + 1;
          break;
      }
      if (json[i].Pensja >= 7100)
        taxesPercent[1][1] = taxesPercent[1][1] + 1;
      else
        taxesPercent[2][1] = taxesPercent[2][1] + 1;
    }
    return [employeesPercent, taxesPercent, carsPercent, overtimePercent];
  })
};

google.charts.load('current', {
  'callback': function () {
    var options = {
      height: 500,
      pieHole: 0.5,
    };

    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    $(document).ready(function() {
      getData();

      $(document).on('change', '#select', function() {
        switch ($('#select').val()) {
          case 'employeesPercent':
            var newData = google.visualization.arrayToDataTable(employeesPercent);
            break;
          case 'taxesPercent':
            var newData = google.visualization.arrayToDataTable(taxesPercent);
            break;
          case 'carsPercent':
            var newData = google.visualization.arrayToDataTable(carsPercent);
            break;
          case 'overtimePercent':
            var newData = google.visualization.arrayToDataTable(overtimePercent);
            break;
        }
        chart.draw(newData, options);
      })
    })
  },
  'packages':['corechart']
});
