//dodanie do diva zawartosci pliku pracownicy.json
function pracownicy() {
  var HTML = '<div class="table-responsive">' +
    '<table class="table" id="pracownicy">' +
    '<thead>' +
    '<tr>' +
    '<th>' + 'ID' + '</th>' +
    '<th>' + 'Imie' + '</th>' +
    '<th>' + 'Nazwisko' + '</th>' +
    '<th>' + 'Stanowisko' + '</th>' +
    '<th>' + 'Nadgodziny' + '</th>' +
    '<th>' + 'Pensja' + '</th>' +
    '<th>' + 'Auto' + '</th>' +
    '<th>' + ' ' + '</th>' +
    '<th>' + ' ' + '</th>' +
    '</tr>' +
    '</thead>';

  $.getJSON("pracownicy.json", function(json) {

    for (i = 0; i < json.length; i++) {
      var id = json[i].ID;
      var name = json[i].Imie;
      var surname = json[i].Nazwisko;
      var position = json[i].Stanowisko;
      var overh = json[i].Nadgodziny;
      var salary = json[i].Pensja;
      var auto = json[i].Auto;

      var data = [id, String(name), String(surname)];


      HTML += '<tbody>' +
        '<tr>' +
        '<td>' + id + '</td>' +
        '<td>' + name + '</td>' +
        '<td>' + surname + '</td>' +
        '<td>' + position + '</td>' +
        '<td>' + overh + '</td>' +
        '<td>' + salary + '</td>' +
        '<td>' + auto + '</td>' +
        '<td>' + '<button type="submit" class="btn btn-default" data-toggle="modal" data-target="#editEmployee" onClick="editEmployee(\'' + id + '\',\'' + name + '\',\'' + surname + '\');">Edytuj</button></form>' + '</td>' +
        '<td>' + '<form action="/users/delete" method="POST"><button type="submit" name="id" value="' + id + '" class="btn btn-default">Usuń</button></form>' + '</td>' +
        '</tr>' +
        '</tbody>';
    }
    HTML += '</table>' +
      '</div>';
    document.getElementById("staff").innerHTML = HTML;
  });
}

//szukanie pracownika pod ID
function search() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("pracownicy");
  tr = table.getElementsByTagName("tr");


  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}




let actualEmployeeId;
let actualEmployeeName;

function editEmployee(id, name, surname) {
  actualEmployeeId = id;
  actualEmployeeName = name + '  ' + surname;

  let HTML = '<input type="hidden" class="form-control" id="edit_id" value="' + actualEmployeeId + '" name="edit_id">';
  document.getElementById("edit_id").innerHTML = HTML;
}

$(document).ready(function() {
  $('#editEmployee').on('shown.bs.modal', function() {
    document.getElementById("employee_name").innerHTML = actualEmployeeName;
    $('#myInput').trigger('focus')
  })
});