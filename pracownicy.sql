-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 21 Maj 2018, 13:07
-- Wersja serwera: 10.1.32-MariaDB
-- Wersja PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `sampledb`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownicy`
--

CREATE TABLE `pracownicy` (
  `ID` int(11) NOT NULL,
  `Imie` varchar(100) NOT NULL,
  `Nazwisko` varchar(100) NOT NULL,
  `Stanowisko` varchar(20) NOT NULL,
  `Nadgodziny` int(11) NOT NULL,
  `Pensja` float NOT NULL,
  `Auto` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `pracownicy`
--

INSERT INTO `pracownicy` (`ID`, `Imie`, `Nazwisko`, `Stanowisko`, `Nadgodziny`, `Pensja`, `Auto`) VALUES
(11, 'Janusz', 'Tracz', 'Dyrektor', 0, 0, 0),
(12, 'Adara', 'Lattey', 'Pracownik', 27, 8900, 1),
(13, 'Gannon', 'Tomik', 'Pracownik', 25, 18246, 1),
(14, 'Deane', 'Dennidge', 'Pracownik', 21, 8902, 0),
(15, 'Lucius', 'Gini', 'Pracownik', 6, 7154, 1),
(16, 'Allyson', 'Ecclestone', 'Pracownik', 7, 17976, 0),
(17, 'Amaleta', 'Bales', 'Pracownik', 38, 18618, 1),
(18, 'Calhoun', 'Maffiotti', 'Pracownik', 21, 17046, 1),
(19, 'Kean', 'Lowson', 'Pracownik', 36, 13664, 0),
(20, 'Kelby', 'Pilipets', 'Pracownik', 3, 16420, 1),
(21, 'Putnem', 'Wigelsworth', 'Pracownik', 39, 3237, 1),
(22, 'Winthrop', 'Vicioso', 'Pracownik', 21, 16424, 0),
(23, 'Boyd', 'Falvey', 'Pracownik', 38, 18726, 1),
(24, 'Roosevelt', 'Lardier', 'Kierownik', 21, 2399, 0),
(25, 'Ariadne', 'Newton', 'Pracownik', 24, 4955, 0),
(26, 'Findlay', 'Witherspoon', 'Pracownik', 3, 14925, 1),
(27, 'Devin', 'Danilov', 'Pracownik', 9, 18665, 1),
(28, 'Niel', 'Harrowing', 'Pracownik', 1, 12636, 0),
(29, 'Elysee', 'Marlon', 'Pracownik', 18, 17708, 0),
(30, 'Sharline', 'Gilhool', 'Pracownik', 17, 6652, 1),
(31, 'Viole', 'Sealy', 'Pracownik', 0, 8706, 0),
(32, 'Delores', 'Attack', 'Pracownik', 20, 13128, 1),
(33, 'Roslyn', 'Tomicki', 'Pracownik', 34, 11196, 1),
(34, 'Ford', 'Sennett', 'Pracownik', 48, 16454, 1),
(35, 'Wilek', 'Olligan', 'Pracownik', 21, 9749, 1),
(36, 'Ericha', 'Relph', 'Pracownik', 41, 3200, 1),
(37, 'Karlotta', 'Trosdall', 'Pracownik', 14, 17407, 0),
(38, 'Roi', 'Greetham', 'Pracownik', 31, 18899, 0),
(39, 'Darcey', 'Ranahan', 'Kierownik', 34, 13529, 1),
(40, 'Lizbeth', 'Symons', 'Pracownik', 46, 5126, 0),
(41, 'Esra', 'Leppingwell', 'Kierownik', 34, 9159, 0),
(42, 'Nina', 'Larcier', 'Pracownik', 20, 19474, 0),
(43, 'Zolly', 'Piscopo', 'Pracownik', 13, 5123, 1),
(44, 'Willi', 'Toyne', 'Pracownik', 41, 18029, 1),
(45, 'Isadora', 'Ricciardello', 'Dyrektor', 24, 9441, 0),
(46, 'Gusta', 'Christol', 'Pracownik', 47, 14510, 0),
(47, 'Correna', 'Barrie', 'Pracownik', 40, 13248, 1),
(48, 'Alick', 'Piotr', 'Pracownik', 17, 16279, 1),
(49, 'Celestine', 'Castagnier', 'Pracownik', 5, 9232, 1),
(50, 'Margery', 'Jukubczak', 'Pracownik', 43, 6111, 0),
(51, 'Alicea', 'Berthomier', 'Pracownik', 8, 14257, 0),
(52, 'Glenna', 'Kincaid', 'Pracownik', 41, 17839, 0),
(53, 'Adelina', 'Hardy', 'Pracownik', 16, 10456, 1),
(54, 'Hillyer', 'Binden', 'Pracownik', 5, 6810, 0),
(55, 'Gwenora', 'Hayhoe', 'Pracownik', 35, 6518, 0),
(56, 'Storm', 'Smoughton', 'Pracownik', 34, 18022, 1),
(57, 'Hunter', 'Willock', 'Pracownik', 24, 13267, 0),
(58, 'Opalina', 'Koene', 'Kierownik', 14, 7267, 0),
(59, 'Arie', 'Demageard', 'Pracownik', 46, 18101, 1),
(60, 'Livia', 'Garnsworthy', 'Pracownik', 19, 7344, 0),
(61, 'Gerhardt', 'Wiper', 'Pracownik', 39, 7080, 1),
(62, 'Sibel', 'Baldinotti', 'Pracownik', 16, 9032, 0),
(63, 'Adara', 'Lattey', 'Pracownik', 27, 8900, 1),
(64, 'Gannon', 'Tomik', 'Pracownik', 25, 18246, 1),
(65, 'Deane', 'Dennidge', 'Pracownik', 21, 8902, 0),
(66, 'Lucius', 'Gini', 'Pracownik', 6, 7154, 1),
(67, 'Allyson', 'Ecclestone', 'Pracownik', 7, 17976, 0),
(68, 'Amaleta', 'Bales', 'Pracownik', 38, 18618, 1),
(69, 'Calhoun', 'Maffiotti', 'Pracownik', 21, 17046, 1),
(70, 'Kean', 'Lowson', 'Pracownik', 36, 13664, 0),
(71, 'Kelby', 'Pilipets', 'Pracownik', 3, 16420, 1),
(72, 'Putnem', 'Wigelsworth', 'Pracownik', 39, 3237, 1),
(73, 'Winthrop', 'Vicioso', 'Pracownik', 21, 16424, 0),
(74, 'Boyd', 'Falvey', 'Pracownik', 38, 18726, 1),
(75, 'Roosevelt', 'Lardier', 'Kierownik', 21, 2399, 0),
(76, 'Ariadne', 'Newton', 'Pracownik', 24, 4955, 0),
(77, 'Findlay', 'Witherspoon', 'Pracownik', 3, 14925, 1),
(78, 'Devin', 'Danilov', 'Pracownik', 9, 18665, 1),
(79, 'Niel', 'Harrowing', 'Pracownik', 1, 12636, 0),
(80, 'Elysee', 'Marlon', 'Pracownik', 18, 17708, 0),
(81, 'Sharline', 'Gilhool', 'Pracownik', 17, 6652, 1),
(82, 'Viole', 'Sealy', 'Pracownik', 0, 8706, 0),
(83, 'Delores', 'Attack', 'Pracownik', 20, 13128, 1),
(84, 'Roslyn', 'Tomicki', 'Pracownik', 34, 11196, 1),
(85, 'Ford', 'Sennett', 'Pracownik', 48, 16454, 1),
(86, 'Wilek', 'Olligan', 'Pracownik', 21, 9749, 1),
(87, 'Ericha', 'Relph', 'Pracownik', 41, 3200, 1),
(88, 'Karlotta', 'Trosdall', 'Pracownik', 14, 17407, 0),
(89, 'Roi', 'Greetham', 'Pracownik', 31, 18899, 0),
(90, 'Darcey', 'Ranahan', 'Kierownik', 34, 13529, 1),
(91, 'Lizbeth', 'Symons', 'Pracownik', 46, 5126, 0),
(92, 'Esra', 'Leppingwell', 'Kierownik', 34, 9159, 0),
(93, 'Nina', 'Larcier', 'Pracownik', 20, 19474, 0),
(94, 'Zolly', 'Piscopo', 'Pracownik', 13, 5123, 1),
(95, 'Willi', 'Toyne', 'Pracownik', 41, 18029, 1),
(96, 'Isadora', 'Ricciardello', 'Dyrektor', 24, 9441, 0),
(97, 'Gusta', 'Christol', 'Pracownik', 47, 14510, 0),
(98, 'Correna', 'Barrie', 'Pracownik', 40, 13248, 1),
(99, 'Alick', 'Piotr', 'Pracownik', 17, 16279, 1),
(100, 'Celestine', 'Castagnier', 'Pracownik', 5, 9232, 1),
(101, 'Margery', 'Jukubczak', 'Pracownik', 43, 6111, 0),
(102, 'Alicea', 'Berthomier', 'Pracownik', 8, 14257, 0),
(103, 'Cristian', 'Roake', 'Pracownik', 38, 15018, 1),
(104, 'Stepha', 'Lattin', 'Pracownik', 28, 9821, 0),
(105, 'Riannon', 'Robertis', 'Pracownik', 46, 3871, 1),
(106, 'Ulrikaumeko', 'Maughan', 'Pracownik', 35, 9374, 1),
(107, 'Gerry', 'Blaine', 'Pracownik', 22, 18443, 0),
(108, 'Carline', 'Tomeo', 'Pracownik', 3, 4815, 1),
(109, 'Ram', 'Vasey', 'Pracownik', 50, 8616, 1),
(110, 'Alexis', 'Beech', 'Pracownik', 42, 13358, 1),
(111, 'Arlyne', 'Tyrrell', 'Pracownik', 18, 5565, 0),
(112, 'Amaleta', 'Hague', 'Pracownik', 40, 9359, 1),
(113, 'Basile', 'Undrell', 'Pracownik', 34, 5956, 0),
(114, 'Zacharie', 'Seer', 'Pracownik', 46, 17325, 1),
(115, 'Kerrin', 'Burbridge', 'Pracownik', 22, 14569, 1),
(116, 'Sherman', 'Rohlf', 'Pracownik', 29, 14756, 0),
(117, 'Deny', 'Gruczka', 'Pracownik', 43, 18809, 1),
(118, 'Betti', 'Waring', 'Pracownik', 45, 16279, 0),
(119, 'Cacilie', 'Kleiner', 'Pracownik', 47, 3360, 0),
(120, 'L;urette', 'Marsy', 'Pracownik', 26, 12276, 0),
(121, 'Randolph', 'Quinton', 'Pracownik', 42, 2177, 0),
(122, 'Jsandye', 'Filkov', 'Dyrektor', 9, 8961, 1),
(123, 'Peyton', 'Wawer', 'Pracownik', 47, 15826, 0),
(124, 'Ralf', 'Newham', 'Pracownik', 35, 7342, 0),
(125, 'Caressa', 'Nimmo', 'Pracownik', 24, 11734, 1),
(126, 'Cari', 'Brickstock', 'Pracownik', 49, 8093, 0),
(127, 'Katlin', 'Badder', 'Pracownik', 38, 6654, 1),
(128, 'Deloria', 'Trippett', 'Pracownik', 30, 12587, 1),
(129, 'Sabina', 'Lebbern', 'Pracownik', 5, 16887, 0),
(130, 'Tanney', 'Kenset', 'Dyrektor', 37, 14164, 0),
(131, 'Elayne', 'Grzegorecki', 'Pracownik', 26, 5817, 1),
(132, 'Margy', 'Gerish', 'Pracownik', 22, 17548, 0),
(133, 'Rand', 'D\'Adda', 'Kierownik', 16, 17032, 0),
(134, 'Marthena', 'Hattam', 'Pracownik', 20, 10011, 0),
(135, 'Adella', 'Botwood', 'Pracownik', 36, 10066, 1),
(136, 'Beau', 'Bradick', 'Pracownik', 23, 15781, 1),
(137, 'Ruddie', 'Rudeyeard', 'Dyrektor', 12, 10525, 1),
(138, 'Lowell', 'Flintiff', 'Pracownik', 33, 5988, 0),
(139, 'Tiffany', 'Eddicott', 'Pracownik', 47, 17130, 1),
(140, 'Luciano', 'Zuanelli', 'Kierownik', 41, 9286, 1),
(141, 'Lorette', 'Wigsell', 'Pracownik', 44, 17744, 1),
(142, 'Kristofor', 'Pentony', 'Pracownik', 4, 5044, 0),
(143, 'Jehanna', 'Blase', 'Dyrektor', 8, 8400, 1),
(144, 'Jacquelyn', 'Fehely', 'Pracownik', 4, 19554, 0),
(145, 'Fredelia', 'Joska', 'Pracownik', 13, 19155, 1),
(146, 'Titus', 'Breydin', 'Dyrektor', 26, 17140, 1),
(147, 'Carlin', 'Sparkwell', 'Pracownik', 42, 19212, 0),
(148, 'Sigismondo', 'Cush', 'Pracownik', 44, 19732, 0),
(149, 'Nissie', 'Perry', 'Pracownik', 29, 9718, 0),
(150, 'Bernita', 'Vaneschi', 'Pracownik', 13, 16515, 1),
(151, 'Cynthy', 'Swaite', 'Pracownik', 26, 6457, 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
