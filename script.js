//dodanie zaleznosci
var express = require('express');
var path = require('path');
var app = express();
var fs = require('fs');
var bodyParser = require('body-parser');
var mysql = require('mysql');

//polaczenie z baza danych MYSQL
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "sampleDB"
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
//dolaczenie wlasnych plikow do programu
app.use('/cssFiles/myStyles.css', express.static(__dirname + './assets'));
app.use('/scriptFiles/main.js', express.static(__dirname + './skrypty'));
app.use(express.static("."));
//dolacznie glownej strony do programu
app.get('/', function(req, resp) {
  generate();
  resp.sendFile('index.html', {
    root: path.join(__dirname, './files')
  });
})
//przejscie do chart.html, generowanie wykresow
app.get('/chart', function(req, resp) {
  resp.sendFile('chart.html', {
    root: path.join(__dirname, './files/views')
  });
})


app.get('/tables', function(req, resp) {
  resp.sendFile('tables.html', {
    root: path.join(__dirname, './files/views')
  });
})

//przejscie do add_employee.html, do dodawnia pracownikow

app.get('/add_employee', function(req, resp) {
  resp.sendFile('add_employee.html', {
    root: path.join(__dirname, './files/views')
  });
})
//przejscie po usunieciu pracownika
app.post('/users/delete', function(req, res) {
  console.log(req.body.id);
  var sql = "DELETE FROM pracownicy WHERE id = " + req.body.id;
  con.query(sql, function(err, result) {
    if (err) throw err;
    console.log("Number of records deleted: " + result.affectedRows);
  });
  generate();
  res.redirect('/');
});

//funkcja zapisuje do pliku JSON, wszystkich pracowników.
function generate() {
  con.query("SELECT * FROM pracownicy", function(err, result, fields) {
    if (err) {
      throw err;
    } else {
      console.log(result);
      fs.writeFile('pracownicy.json', JSON.stringify(result), function(err) {
        if (err) throw err;
        console.log('Saved!');
        fs.readFile('pracownicy.json', 'utf8', function(err, data) {
          if (err) throw err; // we'll not consider error handling for now
          var obj = JSON.parse(data);
        });
      });
    }
  });
}
//wywolanie metody POST do pobrania danych z formularza, który znajduje się w index.html
app.post('/users/add', function(req, res) {
  var name = req.body.name;
  var surname = req.body.surname;
  var select = req.body.select;
  var overh = req.body.overh;
  var salary = req.body.salary;
  var yes = req.body.yes;
  var no = req.body.no;
  var car;

  if (yes === undefined && no == undefined) {
    //dodać alert i zatrzymac dodawanie uzytkownika
    console.log('Nie zostala wybrana żadna opcja');
  }
  if (yes == "yes") {
    car = 1;
  }
  if (no == "no") {
    car = 0;
  }

  //var sql = "INSERT INTO pracownicy (ID, Imie, Nazwisko, Stanowisko, Nadgodziny, Pensja, Auto) VALUES ('', '"+name+"'"+")";
  var sql = "INSERT INTO `pracownicy` (`ID`, `Imie`, `Nazwisko`, `Stanowisko`, `Nadgodziny`, `Pensja`, `Auto`) VALUES (NULL, '" + name + "', '" + surname + "', '" + select + "', '" + overh + "', '" + salary + "', '" + car + "'" + ")";
  con.query(sql, function(err, result) {
    if (err) throw err;
    console.log("1 record inserted");
  });

  //ponowne generowanie listy uzytkonikow po dodaniu nowego
  generate();
  res.redirect('/add_employee');
});

app.post('/users/edit', function(req, res) {
  var name = req.body.edit_name;
  var surname = req.body.edit_surname;
  var select = req.body.edit_select;
  var overh = req.body.edit_overh;
  var salary = req.body.edit_salary;
  var haveCar;

  if (req.body.haveCar == "yes") {
    haveCar = 1;
  } else {
    haveCar = 0;
  }


  var sql = "UPDATE `pracownicy` SET `Imie`= \'" + name + "\' , `Nazwisko`= \'" + surname + "\', `Stanowisko`= \'" + select + "\' ,`Nadgodziny`= \'" + overh + "\',`Pensja`= \'" + salary + "\',`Auto`= \'" + haveCar + "\' WHERE ID=" + req.body.edit_id;
  console.log(sql);
  con.query(sql, function(err, result) {
    if (err) throw err;
    console.log("1 record updated");
  });
  res.redirect('/');
});

//uruchomienie aplikacji
app.listen(1337, function() {
  console.log('Listening at Port 1337');
});